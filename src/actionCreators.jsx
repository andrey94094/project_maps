export function mapMarkers(markers) {
  return {
    type: 'FETCH_MARKERS',
    payload: { markers }
  }
}
export function mapMarker(marker) {
  return {
    type: 'ADD_MARKER',
    payload: { marker }
  }
}