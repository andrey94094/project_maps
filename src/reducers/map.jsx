const initState = {
    markers: [{ lat: 54, lng: 28 }, { lat: 53, lng: 27 }]
}

function reducer(state = initState, action) {
    switch (action.type) {
        case 'FETCH_MARKERS': {
            return {
                markers: action.payload.markers,
            }
        }
        case 'ADD_MARKER': {
            const markers = [...state.markers];
            markers.push(action.payload.marker);
            return {
                markers: markers,
            }
        }
        default: return state;
    }
}

export default reducer;