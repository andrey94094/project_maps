import { combineReducers } from 'redux';
import markers from './map'

export default combineReducers({
    markers,
});
