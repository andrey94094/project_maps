import { PureComponent } from 'react';
import { mapMarker } from '../../actionCreators';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import "./AddMarker.scss"

class AddMarker extends PureComponent {
    state = {
        isLoading: false,
        isShowForm: true,
        lngs: '',
        lats: '',
    };
    handleChangelat = (e) => {
        this.setState({ lats: e.target.value })
    };

    handleChangelng = (e) => {
        this.setState({ lngs: e.target.value })
    };

    handleClick() {
        this.setState({ isShowForm: false });
        const { lngs, lats } = this.state;
        const lat = Number(lats)
        const lng = Number(lngs)
        const { mapMarker } = this.props;
        const marker = { lat, lng};
        mapMarker(marker);
    }

    render() {
        const { isShowForm, isLoading, lats, lngs } = this.state;
        return (
            <section className="form">
                {isShowForm ?
                    (<div>
                        <input className="form__input" placeholder={"широта"} value={lats} pattern="^-?[0-9]\d*$" onChange={this.handleChangelat} name="lat"></input>
                        <input className="form__input" placeholder={"долгота"} value={lngs} pattern="^-?[0-9]\d*$" onChange={this.handleChangelng} name="lng"></input>
                        <button className="form__button" onClick={() => this.handleClick()} disabled={isLoading}>ДОБАВИТЬ</button>
                    </div>
                    ) : `координаты добавены`}
                        <button className="form__button" onClick={()=>this.props.history.push('/')}>На главную</button>
            </section>
        )
    }
}

const mapStateToProps = ({ markers }) => ({
    markers: markers,
});

const mapDispatchToProps = (dispatch) => ({
    mapMarker: (marker) => dispatch(mapMarker(marker)),
});

export default connect(mapStateToProps, mapDispatchToProps) (withRouter(AddMarker));
