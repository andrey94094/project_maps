import React from "react";
import {
  withGoogleMap,
  withScriptjs,
  GoogleMap,
  Marker
} from "react-google-maps";
import { mapMarkers } from "../../actionCreators";
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import './Map.scss'


class Map extends React.Component {
  state = {
    markers: [],
    isShowMore: false,
  };

  componentDidMount = async () => {
    try{
      this.setState({ isShowMore: true })
      const {mapMarkers} = this.props;
      // const response = await fetch(`http://personal.armproject.by/employees`); /// Для приёма данных с сервера,
      // const markers = await response.json(); /// Не реализовывал, так как не знаю ссылки
      // mapMarkers(markers);
      // this.setState({ isShowMore: true })
    }
    catch(err){
    this.setState({ isShowMore: false })
    }
  }

  render = () => {
    const { markers } = this.props;
    const { isShowMore } = this.state;
    return (
      <section className="form">
        {isShowMore &&
          <GoogleMap
            defaultZoom={7}
            defaultCenter={{ lat: 53.9, lng: 27.5667 }}
          >
            {markers.map((item) => (
              <Marker
                position={item}
              />
            ))}
          </GoogleMap>
        }
        <button className="form__button" onClick={() => this.props.history.push('/addmaker')}>Добавить точку</button>
      </section>
    );
  };
}

const mapStateToProps = ({ markers }) => ({
  markers: markers.markers,
});
const mapDispatchToProps = (dispatch) => ({
  mapMarkers: (markers) => dispatch(mapMarkers(markers)),
});

const MapComponent = connect(mapStateToProps, mapDispatchToProps)(withScriptjs(withRouter(withGoogleMap(Map))));

export default () => (
  <MapComponent
    googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places"
    loadingElement={<div className="map__load" />}
    containerElement={<div className="map" />}
    mapElement={<div className ="map__element"/>}
  />
);
