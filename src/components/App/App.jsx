import { PureComponent } from 'react';
import './App.scss';
import { BrowserRouter as Router, Switch, Route, Link, withRouter } from 'react-router-dom';
import Map from '../Maps'
import AddMarker from '../AddMarker';

class App extends PureComponent {
  render() {
    return (
      <Router>
      <main>
      <Link to="/"></Link>
          <Link to="/addmaker"></Link>
          <Switch>
          <Route exact path="/">
        <Map/>
        </Route>
        <Route exact path="/addmaker">
        <AddMarker/>
        </Route>
        </Switch>
      </main>
      </Router>
    )
  }
}


export default withRouter(App);
